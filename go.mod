module gitlab.com/vizzyolog/geminiadvisory

go 1.14

require (
	github.com/gocolly/colly/v2 v2.1.0
	github.com/kr/pretty v0.2.1
	googlemaps.github.io/maps v1.2.3
)
