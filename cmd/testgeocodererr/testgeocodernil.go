package main

import (
	"fmt"

	"gitlab.com/vizzyolog/geminiadvisory/internal/pkg/geocoder"
)

func main() {
	lat, lng, err := geocoder.GetLocation("ошибка")
	if err != nil {
		fmt.Println("err geo", err)
	}
	fmt.Println("lat, lng:", lat, lng)
}
