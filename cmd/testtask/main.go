package main

import (
	"fmt"
	"regexp"
	"strings"

	"gitlab.com/vizzyolog/geminiadvisory/internal/pkg/geocoder"

	"github.com/gocolly/colly/v2"
	"github.com/kr/pretty"
)

var targetSource string = "https://ymcanyc.org/locations?type&amenities"

type branch struct {
	URL      string
	Name     string
	Address  string
	Location latlng
	Phone    string
	Staff    []employee
}
type latlng struct {
	Lat float64
	Lng float64
}

type employee struct {
	Name  string
	Phone string
	Email string
}

var branches []branch
var staffTotal int

func main() {

	branchC := colly.NewCollector(
		colly.AllowedDomains("ymcanyc.org", "www.ymcanyc.org"),
		colly.CacheDir("./ymcanyc.org"),
	)

	//Getting list of branches
	branchC.OnHTML("div.location-list-item", func(e *colly.HTMLElement) {

		bURL := e.ChildAttr(`a.btn-primary`, `href`)
		bURLA := e.Request.AbsoluteURL(bURL + "/about")

		e.Request.Ctx.Put("branchName", e.ChildText(`h2.location-item--title.card-type--branch`))
		e.Request.Ctx.Put("branchArea", e.ChildText(`div.field-borough`))
		e.Request.Ctx.Put("branchAddress", e.ChildText(`div.field-location-direction`))
		e.Request.Ctx.Put("branchPhone", e.ChildText(`div.field-location-phone.field-item`))

		branchC.Visit(bURLA)

	})

	//Getting list of staff case A
	branchC.OnHTML("div.left-col.col-12.col-sm-6", func(e *colly.HTMLElement) {

		branchStaff := parseStaff(e)

		if len(branchStaff) > 0 {
			saveBranch(e, branchStaff)
		}

	})

	//Getting list of staff case B
	branchC.OnHTML("div.field-prgf-description.field-item", func(e *colly.HTMLElement) {

		branchStaff := parseStaff(e)

		if len(branchStaff) > 0 {
			saveBranch(e, branchStaff)
		}
	})

	//Start scraping
	branchC.Visit(targetSource)

	//Result
	pretty.Println(branches)

	//Checking sum
	fmt.Println("branchesTotal", len(branches))
	fmt.Println("staffTotal", staffTotal)
}

//Saving branch with stuff and GEOlocation
func saveBranch(e *colly.HTMLElement, staff []employee) {
	var newBranch branch
	var err error
	newBranch.URL = e.Request.URL.String()
	newBranch.Name = e.Request.Ctx.Get("branchName")
	newBranch.Address = e.Request.Ctx.Get("branchAddress")
	newBranch.Phone = e.Request.Ctx.Get("branchPhone")

	//Getting location by address from google geocoder
	var newLocation latlng
	newLocation.Lat, newLocation.Lng, err = geocoder.GetLocation(newBranch.Address)
	if err != nil {
		fmt.Println("err get location for address", err)
	}
	newBranch.Location = newLocation

	//Adding parsed staf
	newBranch.Staff = staff
	staffTotal = staffTotal + len(staff)
	//Branch to branches
	branches = append(branches, newBranch)
}

//Parsing staff
func parseStaff(e *colly.HTMLElement) (branchStaff []employee) {
	if strings.Contains(e.Text, "Staff") || strings.Contains(e.Text, "Leadership") {
		switch url := e.Request.URL.String(); url {
		//exeption1 with bad layout name and email in different <p>
		case "https://ymcanyc.org/locations/mcburney-ymca/about":

			e.ForEach("p", func(_ int, p *colly.HTMLElement) {

				var newEmployee employee
				var name string
				var email string

				if email = p.DOM.Find("a").Text(); email != "" {
					name = p.DOM.Prev().Prev().Text()
				}
				if name != "" {
					newEmployee.Name = name
					newEmployee.Email = email

					branchStaff = append(branchStaff, newEmployee)
				}
			})
		//exeption2 with bad layout iinverted markup logic of name and position
		case "https://ymcanyc.org/locations/coney-island-ymca/about":

			e.ForEach("p", func(_ int, p *colly.HTMLElement) {

				var newEmployee employee

				newEmployee.Name = p.Text
				notname := p.ChildText("strong")
				newEmployee.Name = strings.Replace(newEmployee.Name, notname, "", 1)

				if newEmployee.Name != "" {
					branchStaff = append(branchStaff, newEmployee)
				}
			})

		default:
			//For all other branches
			e.ForEach("p", func(_ int, p *colly.HTMLElement) {
				var newEmployee employee

				if findingName := p.ChildText("strong"); findingName != "" {
					newEmployee.Name = findingName
				}
				if findingName := p.ChildText("b"); findingName != "" {
					newEmployee.Name = findingName
				}
				if newEmployee.Name != "" {
					newEmployee.Email = p.ChildText("a")

					findingPhone := regexp.MustCompile(`[0-9]{3}-[0-9]{3}-[0-9]{4} x[0-9]{4}|[0-9]{3}-[0-9]{3}-[0-9]{4} x [0-9]{4}|[0-9]{3}-[0-9]{3}-[0-9]{4} x [0-9]{4}|[0-9]{3}-[0-9]{3}-[0-9]{4}`)
					newEmployee.Phone = findingPhone.FindString(p.Text)

					branchStaff = append(branchStaff, newEmployee)
				}
			})

		}
	}
	return
}
