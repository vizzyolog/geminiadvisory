//Package geocoder based on this example
//https://github.com/googlemaps/google-maps-services-go/blob/master/examples/geocoding/cmdline/main.go
package geocoder

import (
	"context"
	"errors"
	"fmt"
	"io/ioutil"

	"googlemaps.github.io/maps"
)

//getGoogleAPIKey - read api key from disk
func getGoogleAPIKey() (string, error) {

	data, err := ioutil.ReadFile("../../myGoogleApiKey")
	if err != nil {

		return "", err
	}

	return string(data), nil
}

//GetLocation - make request to google service with address
func GetLocation(requestAddress string) (float64, float64, error) {
	var client *maps.Client
	var err error

	myAPIKey, err := getGoogleAPIKey()
	if err != nil {
		fmt.Printf("err Read File %v \n", err)
		return 0, 0, err
	}
	client, err = maps.NewClient(maps.WithAPIKey(myAPIKey))
	if err != nil {
		fmt.Println("Google NewClient err ", err)
		return 0, 0, err
	}
	r := &maps.GeocodingRequest{
		Address: requestAddress,
	}

	result, err := client.Geocode(context.Background(), r)
	if err != nil {
		fmt.Println("Geocode err: ", err)
		return 0, 0, err
	}
	if len(result) > 0 {
		lat := result[0].Geometry.Location.Lat
		lng := result[0].Geometry.Location.Lng
		//pretty.Println(result)

		return lat, lng, err
	}

	return 0, 0, errors.New("zero length in result from google")
}
